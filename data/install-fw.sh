#!/bin/sh

set -e

echo "Copying device firmware."

root_dev=/dev/mmcblk0p3
root_vol=/mnt/chrome_root

if [ ! -d ${root_vol} ]; then
    mkdir -p ${root_vol}
fi

if mount | grep "on ${root_vol} type" > /dev/null
then
    umount $root_vol
fi

mount -o,ro ${root_dev} ${root_vol}

# Copy the non-free firmware for the wifi device:
mkdir -p /lib/firmware/
cp -r -n ${root_vol}/lib/firmware/* /lib/firmware

umount ${root_dev}

echo "All done."

exit 0
