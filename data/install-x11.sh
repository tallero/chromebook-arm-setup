#!/bin/bash

set -e

echo "Installing X11 and development packages"
apt-get update
apt-get install -y xserver-xorg xinit openbox xfce4-terminal \
   build-essential git autogen automake autoconf libtool xorg-dev xutils-dev \
   libdrm-dev libudev-dev libxcb-dri2-0

echo "Compiling xf86-video-armsoc driver"
cd /root
git clone git://anongit.freedesktop.org/xorg/driver/xf86-video-armsoc
cd xf86-video-armsoc
git checkout 36709c64a70a2891f7aa027c689740125ae7dc54
./autogen.sh --prefix=/usr
make CFLAGS="-w"
make install

echo "alias runX='startx -- :0 vt12'" >> ~/.bashrc

echo "All done.  You may now remove the xf86-video-armsoc directory."
echo "To start X11, reload your .bashrc (source ~/.bashrc) and run: runX"

exit 0
