#!/bin/sh

set -e

echo "Installing Weston and library packages."
apt-get update
apt-get install -y weston libgl1-mesa-dri libgles2-mesa \
libxcb-composite0 libxcb-xkb1

echo "Adding Weston start scripts."

echo "#!/bin/sh
LD_LIBRARY_PATH=/root/mali/wayland:\$LD_LIBRARY_PATH \
XDG_RUNTIME_DIR=\$(mktemp -d) \
weston --backend=drm-backend.so --tty=1 --idle-time=0" \
> /usr/bin/runweston-drm
chmod +x /usr/bin/runweston-drm

echo "#!/bin/sh
LD_LIBRARY_PATH=/root/mali/fbdev:\$LD_LIBRARY_PATH \
XDG_RUNTIME_DIR=\$(mktemp -d) \
weston --backend=fbdev-backend.so --use-gl --tty=1 --idle-time=0" \
> /usr/bin/runweston-fbdev
chmod +x /usr/bin/runweston-fbdev

echo "All done."
echo "To start weston with DRM backend, run: runweston-drm"
echo "To start weston with fbdev backend, run: runweston-fbdev"

exit 0
