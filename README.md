# Linux on Chromebook with ARM Mali GPU

This script have been written by [Guillaume Tucker](https://gtucker.io) and published on the [ARM Community portal](https://community.arm.com/graphics/b/blog/posts/linux-on-chromebook-with-arm-mali-gpu). The one posted on the portal has broken links.
To what I have understand, this script will:

* format a device;
* put the latest [ALIP](https://wiki.linaro.org/Platform/DevPlatform/Rootfs) linaro rootfs ([16.09](http://releases.linaro.org/debian/images/blend-armhf/)) on the second partition of the device;
* get the mali userspace drivers from [ARM](https://developer.arm.com/products/software/mali-drivers/user-space);
* get a kernel compatible with such driver from [chromium](https://chromium.googlesource.com/chromiumos/third_party/kernel), which is any case really old (3.4.x for XE303C12);
* apply patches to the kernel;
* build chromium vboot;

## The problem

I can't figure out how to get past the `build_vboot` stage on Archlinux. The errors I am getting are:

    In file included from firmware/lib/cryptolib/include/cryptolib.h:13,
                     from host/lib/host_key.c:20:
    firmware/lib/cryptolib/include/sha.h:45:3: error: conflicting types for ‘SHA256_CTX’
     } SHA256_CTX;
       ^~~~~~~~~~
    In file included from /usr/include/openssl/x509.h:30,
                     from /usr/include/openssl/pem.h:17,
                     from /usr/include/openssl/ui.h:19,
                     from /usr/include/openssl/engine.h:24,
                     from host/lib/host_key.c:11:
    /usr/include/openssl/sha.h:56:3: note: previous declaration of ‘SHA256_CTX’ was here
     } SHA256_CTX;
       ^~~~~~~~~~
    In file included from firmware/lib/cryptolib/include/cryptolib.h:13,
                     from host/lib/host_key.c:20:
    firmware/lib/cryptolib/include/sha.h:53:3: error: conflicting types for ‘SHA512_CTX’
     } SHA512_CTX;
       ^~~~~~~~~~
    In file included from /usr/include/openssl/x509.h:30,
                     from /usr/include/openssl/pem.h:17,
                     from /usr/include/openssl/ui.h:19,
                     from /usr/include/openssl/engine.h:24,
                     from host/lib/host_key.c:11:
    /usr/include/openssl/sha.h:103:3: note: previous declaration of ‘SHA512_CTX’ was here
     } SHA512_CTX;
       ^~~~~~~~~~
        CC            host/lib/signature_digest.o
    make: *** [Makefile:956: /data/Software/mali_chromebook-setup_006/vboot/build/host/lib/host_key.o] Error 1
    make: *** Waiting for unfinished jobs....
    In file included from firmware/lib/cryptolib/include/cryptolib.h:13,
                     from host/lib/host_signature.c:21:
    firmware/lib/cryptolib/include/sha.h:45:3: error: conflicting types for ‘SHA256_CTX’
     } SHA256_CTX;
       ^~~~~~~~~~
    In file included from /usr/include/openssl/x509.h:30,
                     from /usr/include/openssl/pem.h:17,
                     from /usr/include/openssl/ui.h:19,
                     from /usr/include/openssl/engine.h:24,
                     from host/lib/host_signature.c:11:
    /usr/include/openssl/sha.h:56:3: note: previous declaration of ‘SHA256_CTX’ was here
     } SHA256_CTX;
       ^~~~~~~~~~
    In file included from firmware/lib/cryptolib/include/cryptolib.h:13,
                     from host/lib/host_signature.c:21:
    firmware/lib/cryptolib/include/sha.h:53:3: error: conflicting types for ‘SHA512_CTX’
     } SHA512_CTX;
       ^~~~~~~~~~
    In file included from /usr/include/openssl/x509.h:30,
                     from /usr/include/openssl/pem.h:17,
                     from /usr/include/openssl/ui.h:19,
                     from /usr/include/openssl/engine.h:24,
                     from host/lib/host_signature.c:11:
    /usr/include/openssl/sha.h:103:3: note: previous declaration of ‘SHA512_CTX’ was here
     } SHA512_CTX;
       ^~~~~~~~~~
    make: *** [Makefile:955: /data/Software/mali_chromebook-setup_006/vboot/build/host/lib/host_signature.o] Error 1
    In file included from firmware/lib/cryptolib/include/cryptolib.h:13,
                     from host/lib/signature_digest.c:16:
    firmware/lib/cryptolib/include/sha.h:45:3: error: conflicting types for ‘SHA256_CTX’
     } SHA256_CTX;
       ^~~~~~~~~~
    In file included from /usr/include/openssl/x509.h:30,
                     from /usr/include/openssl/pem.h:17,
                     from /usr/include/openssl/ui.h:19,
                     from /usr/include/openssl/engine.h:24,
                     from host/lib/signature_digest.c:8:
    /usr/include/openssl/sha.h:56:3: note: previous declaration of ‘SHA256_CTX’ was here
     } SHA256_CTX;
       ^~~~~~~~~~
    In file included from firmware/lib/cryptolib/include/cryptolib.h:13,
                     from host/lib/signature_digest.c:16:
    firmware/lib/cryptolib/include/sha.h:53:3: error: conflicting types for ‘SHA512_CTX’
     } SHA512_CTX;
       ^~~~~~~~~~
    In file included from /usr/include/openssl/x509.h:30,
                     from /usr/include/openssl/pem.h:17,
                     from /usr/include/openssl/ui.h:19,
                     from /usr/include/openssl/engine.h:24,
                     from host/lib/signature_digest.c:8:
    /usr/include/openssl/sha.h:103:3: note: previous declaration of ‘SHA512_CTX’ was here
     } SHA512_CTX;

In case someone you know how to fix this or how to properly install an upstream kernel keeping video acceleration on wayland (Xorg is a no-no, it seems), you are welcome.
